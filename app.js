const { faker } = require("@faker-js/faker");
const fs = require("fs");

const generateImages = (number) => {
  const images = [];

  while (number) {
    const value = faker.image.image();
    images.push(value);
    number--;
  }

  console.log(images);

  return images;
};

const generateDate = () => {
  return faker.date.future();
};

const generateParagraphs = (number) => {
  return faker.lorem.paragraph(number);
};

const generateUsers = (number) => {
  const persons = [];

  while (number) {
    persons.push({
      id: faker.datatype.uuid(),
      name: faker.name.firstName(),
      description: generateParagraphs(1),
      picture: faker.image.avatar(),
      country: faker.address.country(),
      joining_date: faker.date.future(),
    });

    number--;
  }

  return persons;
};

// generateImages(5);
// console.log(generateDate());
// console.log(generateParagraphs(3));

fs.writeFileSync(
  "./db.json",
  `{ "users": ${JSON.stringify(generateUsers(13))} }`
);
